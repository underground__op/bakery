<?php
	ob_start();
	session_start();
	include_once ("../model/dbconn.php");
	
	// if session is not set this will redirect to login page
	if( !isset($_SESSION['user']) ) {
		header("Location: login.php");
		exit;
	}
	
	else{
		
		header("Location: payment.php");
		
	}
	
?>