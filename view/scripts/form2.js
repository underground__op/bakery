var const_name = /^[A-Za-z0-9 ]{3,20}$/;
var const_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i 
var const_username = /^[A-Za-z0-9_]{1,20}$/;
var const_password =  /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
var const_phoneno 	= /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
var const_card  	= /^\(?([0-11]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
//var dateReg = /^(([0-9])|([0-2][0-9])|([3][0-1]))\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s\d{4}$/;
var const_address = /^[0-9a-zA-Z]+$/;  
var const_img = /^.*\.(jpg|jpeg|gif|JPG|png|PNG)$/;

function validate(form){
var name = form.name.value;
var email = form.email.value;
var pass = form.pass.value;
var country=form.country.value;
var city=form.city.value;
var phone=form.phone.value;
var add=form.add.value;

 var errors = [];
 

  
 if (!const_name.test(name)) {
  errors[errors.length] = "Your valid Name .";
 }
 
  if (!const_password.test(pass)) {
  errors[errors.length] = "Your valid Password.";
 }
 
 if (!const_email.test(email)) {
  errors[errors.length] = "You must enter a valid email address.";
 }
 if(country == 00){
	 errors[errors.length] = "choose country";
 }
 
  if (!const_name.test(city)) {
  errors[errors.length] = "Enter Valid city .";
 }
 
 if (!const_phoneno.test(phone)) {
  errors[errors.length] = "Your valid Phone.";
 }
 
 if (add.length==0) {
  errors[errors.length] = "Your valid address .";
 }

 if (errors.length > 0) {
  reportErrors(errors);
  return false;
 }else{
	
 }
 
  
  
  return true;
 
 
}


function reportErrors(errors){
 var msg = "Please Enter Valide Data...\n";
 for (var i = 0; i<errors.length; i++) {
  var numError = i + 1;
  msg += "\n" + numError + ". " + errors[i];
 }
 alert(msg);
 
}