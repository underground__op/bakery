<?php
class DBController {
	private $host = "localhost";
	private $user = "root";
	private $password = "";
	private $database = "shoppn";
	private $conn;
	private $ip;
	
	function __construct() {
		$this->conn = $this->connectDB();
	}
	
	function connectDB() {
		$conn = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $conn;
	}
	
	function runQuery($query) {
		$result = mysqli_query($this->conn,$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset))
			return $resultset;
	}
	
	function numRows($query) {
		$result  = mysqli_query($this->conn,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
	
	function insert($query) {
		
		mysqli_query($this->conn,$query);
		return true;
		/* $this->conn->exec($query);
        return  "Insert sucessful_2";*/
	}
	
	// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

// Function to get the client IP address
function get_client_ip2() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
//get a single value
function getSingleValue($tableName, $prop, $value, $columnName){
		
		$result="";
		$car="";
		$q="";
		$f="";
		$name="";
		$value2="";
		
$value_array = $this->runQuery(("SELECT `$columnName` FROM `$tableName` WHERE $prop='".$value."'"));	

if (is_array($value_array) || is_object($value_array)){
	
	foreach($value_array as $item){
		
		$value2 = $item["$columnName"];
		
	}
}	
 // $q = $this->conn->query;
 // $f = $q->fetch();
 // $result = $f[$columnName];
  return $value2;
}

function getDoubleValue($tableName, $prop,$prop1, $value,$value1, $columnName){
		
		$result="";
		$car="";
		$q="";
		$f="";
		$name="";
		$value2="";
		
		$value_array = $this->runQuery(("SELECT `$columnName` FROM `$tableName` WHERE $prop ='$value' AND `$prop1`='$value1'"));
		
//$value_array = $this->runQuery((" SELECT `$columnName` FROM `$tableName` WHERE $prop='".$value."'.AND.$prop1='".$value1."' "));	

if (is_array($value_array) || is_object($value_array)){
	
	foreach($value_array as $item){
		
		$value2 = $item["$columnName"];
		
	}
}	
 // $q = $this->conn->query;
 // $f = $q->fetch();
 // $result = $f[$columnName];
  return $value2;
}

function getCount(){
$this->ip = $this->get_client_ip();

$sql ="SELECT * FROM cart where ip_add ='$this->ip'"; 
/*$result = $conn->query($sql);
$row = $result->fetchAll(PDO::FETCH_ASSOC);*/
$cart_array = $this->runQuery($sql);
$total=0; $count=0;

if (is_array($cart_array) || is_object($cart_array)){
foreach($cart_array as $item){        //loop code
	
	$id = $item['p_id'];
	
    $count++;	
	
	
}	
}
	return $count;
	
}

function getTotal(){
$this->ip = $this->get_client_ip();

$sql ="SELECT * FROM cart where ip_add ='$this->ip'"; 
/*$result = $conn->query($sql);
$row = $result->fetchAll(PDO::FETCH_ASSOC);*/
$cart_array = $this->runQuery($sql);
$total=0; $count=0;

if (is_array($cart_array) || is_object($cart_array)){
foreach($cart_array as $item){        //loop code
	
	$id = $item['p_id'];
   $price=$this->getSingleValue('products', 'product_id', $id, 'product_price');
	 $qty=$this->getDoubleValue('cart', 'p_id','ip_add', $id,$this->ip,'qty');
    $total= $total +($price*$qty);
    
	
}
}

	return $total;

}



}

?>

