-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2018 at 01:23 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoppn`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(2) NOT NULL,
  `brand_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`) VALUES
(1, 'vodka'),
(2, 'ciroc');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(255) NOT NULL,
  `qty` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(2) NOT NULL,
  `cat_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`) VALUES
(1, 'expensive'),
(2, 'cheap');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) NOT NULL,
  `customer_ip` varchar(255) NOT NULL,
  `customer_name` text NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_pass` varchar(100) NOT NULL,
  `customer_country` text NOT NULL,
  `customer_city` text NOT NULL,
  `customer_contact` varchar(100) NOT NULL,
  `customer_image` text NOT NULL,
  `customer_address` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_ip`, `customer_name`, `customer_email`, `customer_pass`, `customer_country`, `customer_city`, `customer_contact`, `customer_image`, `customer_address`) VALUES
(1, '::1', 'Kelvin Degbotse', 'kelvin@h.com', '8ac4f7ac8f5fdd05a6095e82dcd52612bf4c9c255b3936da590611cd2ab765a4', 'Antigua and Barbuda', 'sssssssssss', '1234567890', 'null', 'jjjjjjjjjjjjjjjj'),
(2, '::1', 'Kelvin Degbotse', 'kelvxin@h.com', '17756315ebd47b7110359fc7b168179bf6f2df3646fcc888bc8aa05c78b38ac1', 'Antarctica', 'sssssssssss', '1234567890', 'null', 'jjjjjjjjjjjjjjjj'),
(3, '::1', 'Kelvin Degbotse', 'kelvin@p.com', '891e12e156d8c6609c6d5f3e04b2fc8da6d9ff3d7e9f906314c0909da69637eb', 'Antarctica', 'sssssssssss', '1234567890', 'null', 'jjjjjjjjjjjjjjjj'),
(4, '::1', 'Kelvin Degbotse', 'kelvin@u.com', '8ac4f7ac8f5fdd05a6095e82dcd52612bf4c9c255b3936da590611cd2ab765a4', 'Angola', 'ham', '1234567890', 'null', 'jjjjjjjjjjjjjjjj'),
(5, '::1', 'Kelvin Degbotse', 'kelvin@pp.com', '8ac4f7ac8f5fdd05a6095e82dcd52612bf4c9c255b3936da590611cd2ab765a4', 'Antigua and Barbuda', 'okkkkkkkkkkk', '1234567890', 'null', 'aaaaaaaaaaaaaaaaaa'),
(6, '::1', 'Kelvin Degbotse', 'kelvin@hhh.com', '8ac4f7ac8f5fdd05a6095e82dcd52612bf4c9c255b3936da590611cd2ab765a4', 'Antigua and Barbuda', 'www', '1234567890', 'null', 'kkkkkkkkkkkkkk'),
(7, '::1', 'kkd', 'kelvin@op.com', '28cb017dfc99073aa1b47c1b30f413e3ce774c4991eb4158de50f9dbb36d8043', '00', 'jdnddd', '1234567890', 'null', 'aaaaaaaaaaaaaaaaa'),
(8, '::1', 'kkd', 'kelvsin@op.com', 'f24abc34b13fade76e805799f71187da6cd90b9cac373ae65ed57f143bd664e5', '00', 'jdnddd', '1234567890', 'null', 'aaaaaaaaaaaaaaaaa'),
(9, '::1', 'kkd', 'kelvsssin@op.com', 'e2aa442c7891b1523fbf2d33991a1b5fec060f73fa20fd88aebfd6b7a3249532', 'Argentina', 'jdnddd', '1234567890', 'null', 'aaaaaaaaaaaaaaaaa'),
(10, '::1', 'kkd', 'aaaaaaaaaaa@op.com', '71a1f8aa6feb8637f30437ddffc4a74f98759cbb5de749bd9a3ef718590b970a', 'Argentina', 'jdnddd', '1234567890', 'null', 'aaaaaaaaaaaaaaaaa'),
(11, '::1', 'kkkk', 'kelvin@k.com', 'b1ccc1a1221a5de4ab217c5eeffa51e36b3e7190117bcfd4278fa608bd720d41', 'Antigua and Barbuda', 'ssssssssssssss', '1234567890', 'null', 'sssssssssssssss'),
(12, '::1', 'kkkk', 'kelvin@jhm.com', '9c3b67ceb6598304a7222ef0e6f52007bec085f74a9311c1420085f206bc1873', 'Anguilla', 'aaaaaaaaaaaaaa', '1234567890', 'null', 'aaaaaaaaaaaaaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(2) NOT NULL,
  `product_cat` int(2) NOT NULL,
  `product_brand` int(2) NOT NULL,
  `product_title` varchar(25) NOT NULL,
  `product_price` int(2) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_brand`, `product_title`, `product_price`, `product_desc`, `product_image`, `product_keywords`) VALUES
(18, 1, 2, 'Strawberry Cake', 43, '	Cake with Starwberry\r\n	', 'http://localhost/ecom4/view/image/products/download (3).jpg', 'sraw berry cake'),
(19, 1, 2, 'Pink Top Cake', 45, 'Cake with pink toppings\r\n	', 'http://localhost/ecom4/view/image/products/download.jpg', 'Pink toppings cake'),
(20, 1, 2, 'Yellow toppings cake', 65, '	yellow toppins\r\n	', 'http://localhost/ecom4/view/image/products/download (2).jpg', 'yellow toppings cake'),
(21, 1, 2, 'flat browny cake', 76, '	flat browny cake\r\n	', 'http://localhost/ecom4/view/image/products/images.jpg', 'falt browny cake'),
(23, 1, 1, 'multi cake', 56, '	multi cake\r\n	', 'http://localhost/ecom4/view/image/products/download (1).jpg', 'multi cake');

-- --------------------------------------------------------

--
-- Table structure for table `tblproduct`
--

CREATE TABLE `tblproduct` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `price` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproduct`
--

INSERT INTO `tblproduct` (`id`, `name`, `code`, `image`, `price`) VALUES
(1, '3D Camera', '3DcAM01', 'product-images/camera.jpg', 1500.00),
(2, 'External Hard Drive', 'USB02', 'product-images/external-hard-drive.jpg', 800.00),
(3, 'Wrist Watch', 'wristWear03', 'product-images/watch.jpg', 300.00),
(899, 'test', 'test', 'product-images/watch.jpg', 50.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`p_id`,`ip_add`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `product_brand` (`product_brand`),
  ADD KEY `product_cat` (`product_cat`);

--
-- Indexes for table `tblproduct`
--
ALTER TABLE `tblproduct`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tblproduct`
--
ALTER TABLE `tblproduct`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=900;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`product_brand`) REFERENCES `brands` (`brand_id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`product_cat`) REFERENCES `categories` (`cat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
