<?php
	ob_start();
	session_start();
	include_once ("../model/dbconn.php");
$db_handle = new DBController();
	
	// it will never let you open index(login) page if session is set
	if ( isset($_SESSION['user'])!="" ) {
		header("Location: home.php");
		exit;
	}
	
	$email="";

	
	
	$nameError="";
$emailError="";
$passError="";

	$error = false;
	
	if( isset($_POST['btn-login']) ) {	
		
		// prevent sql injections/ clear user invalid inputs
		$email = trim($_POST['email']);
		$email = strip_tags($email);
		$email = htmlspecialchars($email);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		// prevent sql injections / clear user invalid inputs
		
		if(empty($email)){
			$error = true;
			$emailError = "Please enter your email address.";
		} else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$emailError = "Please enter valid email address.";
		}
		
		if(empty($pass)){
			$error = true;
			$passError = "Please enter your password.";
		}
		
		// if there's no error, continue to login
		if (!$error) {
			
			$password = hash('sha256', $pass); // password hashing using SHA256
		
			$query = "SELECT customer_id, customer_name, customer_pass FROM customer WHERE customer_email='$email'";
			//$res=mysql_query("SELECT customer_id, customer_name, customer_pass FROM customer WHERE customer_email='$email'");
			//$row=mysql_fetch_array($res);
			$row = $db_handle->runQuery($query);
			//$count = mysql_num_rows($res); // if uname/pass correct it returns must be 1 row
			$count = $db_handle->numRows($query);
			
			$p=$db_handle->getSingleValue("customer", "customer_email", $email, "customer_pass");
			$i=$db_handle->getSingleValue("customer", "customer_email", $email, "customer_id");
			
			if( $count == 1 && $p==$password ) {
				$_SESSION['user'] = $i;
				header("Location: home.php");
				exit;
			} else {
				$errMSG = "Incorrect Credentials, Try again...";
			}
				
		}
		
	}
?>