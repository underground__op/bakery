<?php
	ob_start();
	session_start();
	if( isset($_SESSION['user'])!="" ){
		header("Location: home.php");
	}
	include_once ("../model/dbconn.php");
$db_handle = new DBController();
	$error = false;
	
$ip = $db_handle->get_client_ip();

$nameError="";
$emailError="";
$passError="";
$countryError="";
$cityError="";
$phoneError="";
$addError="";

$name="";
$email="";
$pass="";
$country="";
$city="";
$phone="";
$add="";

	if ( isset($_POST['btn-signup']) ) {
		
		// clean user inputs to prevent sql injections
		$name = trim($_POST['name']);
		$name = strip_tags($name);
		$name = htmlspecialchars($name);
		
		$email = trim($_POST['email']);
		$email = strip_tags($email);
		$email = htmlspecialchars($email);
		
		$pass = trim($_POST['pass']);
		$pass = strip_tags($pass);
		$pass = htmlspecialchars($pass);
		
		$country = $_POST['country'];
		
		$city = trim($_POST['city']);
		$city = strip_tags($city);
		$city = htmlspecialchars($city);
		
		$phone = trim($_POST['phone']);
		$phone = strip_tags($phone);
		$phone = htmlspecialchars($phone);
		
		$add = trim($_POST['add']);
		$add = strip_tags($add);
		$add = htmlspecialchars($add);
		
		// basic name validation
		if (empty($name)) {
			$error = true;
			$nameError = "Please enter your full name.";
		} else if (strlen($name) < 3) {
			$error = true;
			$nameError = "Name must have atleat 3 characters.";
		} else if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
			$error = true;
			$nameError = "Name must contain alphabets and space.";
		}
		
		//basic email validation
		if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
			$error = true;
			$emailError = "Please enter valid email address.";
		} else {
			// check email exist or not
			$query = "SELECT customer_email FROM customer WHERE customer_email='$email'";
			//$result = mysql_query($query);
			$count = $db_handle->numRows($query);
			if($count!=0){
				$error = true;
				$emailError = "Provided Email is already in use.";
			}
		}
		// password validation
		if (empty($pass)){
			$error = true;
			$passError = "Please enter password.";
		} else if(strlen($pass) < 6) {
			$error = true;
			$passError = "Password must have atleast 6 characters.";
		}
		
		//country validation
		if (empty($country)) {
			$error = true;
			$countryError = "Please choose a country";
		}
		
			// basic city validation
		if (empty($city)) {
			$error = true;
			$cityError = "Please enter your city.";
		} else if (strlen($city) < 3) {
			$error = true;
			$cityError = "city must have atleat 3 characters.";
		} else if (!preg_match("/^[a-zA-Z ]+$/",$city)) {
			$error = true;
			$cityError = "city must contain alphabets and space.";
		}
		
			// phone validation
		if (empty($phone)){
			$error = true;
			$phoneError = "Please enter phoneNumber.";
		} else if(strlen($phone) != 10) {
			$error = true;
			$phoneError = "number must have 10 characters.";
		}
		
		// basic address validation
		if (empty($add)) {
			$error = true;
			$addError = "Please enter your full address.";
		} else if (strlen($add) < 3) {
			$error = true;
			$addError = "Address must have atleat 3 characters.";
		} else if (!preg_match("/^[a-zA-Z ]+$/",$add)) {
			$error = true;
			$addError = "address must contain alphabets and space.";
		}
		// password encrypt using SHA256();
		$password = hash('sha256', $pass);
		
		// if there's no error, continue to signup
		if( !$error ) {
			
			$query = "INSERT INTO customer(customer_ip,customer_name,customer_email,customer_pass,customer_country,customer_city,customer_contact,customer_image,customer_address)
			VALUES('$ip','$name','$email','$password','$country','$city','$phone','null','$add')";
			$res = $db_handle->insert($query);
				
			if ($res) {
				$errTyp = "success";
				$errMSG = "Successfully registered, you may login now";
				unset($name);
				unset($email);
				unset($pass);
				$name="";
				$email="";
			} else {
				$errTyp = "danger";
				$errMSG = "Something went wrong, try again later...";	
			}	
				
		}
		
		
	}
?>